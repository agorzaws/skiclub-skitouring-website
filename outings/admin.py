from django.contrib import admin, messages
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.conf import settings
from django.db.models import Q, Max

from django.contrib import messages
from django.utils.translation import ngettext

from .models import *


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    model = Season
    list_display = ['name','importance' ]
    fields = [
        'name','importance'
    ]


class OutingTeamInline(admin.TabularInline):
    model = OutingTeam
    fields = [
        'organiser',
        'member',
        'status',
        'present',]


@admin.register(Outing)
class OutingAdmin(admin.ModelAdmin):

    def set_COVID_related_note(self, request, queryset):
        note = "Tentatively cancelled due to the COVID related guidelines from the FFS."
        updated = queryset.update(note=note)
        self.message_user(request, ngettext(
            '%d outing\'s note was updated .',
            '%d outings\' notes were updated',
            updated,
        ) % updated, messages.SUCCESS)

    def set_five_participants(self, request, queryset):
        nbParticipants = 5
        updated = queryset.update(max_participants=nbParticipants)
        self.message_user(request, ngettext(
            '%d outing was set to five participants.',
            '%d outings were set to five participants',
            updated,
        ) % updated, messages.SUCCESS)

    def move_to_OK_status(self, request, queryset):
        status = Status.objects.filter(name='OK')[0]
        updated = queryset.update(status=status)
        self.message_user(request, ngettext(
            '%d outing was set to OK status.',
            '%d outings were set to OK status',
            updated,
        ) % updated, messages.SUCCESS)

    def move_to_DRAFT_status(self, request, queryset):
        status = Status.objects.filter(name='DRAFT')[0]
        updated = queryset.update(status=status)
        self.message_user(request, ngettext(
            '%d outing was set to DRAFT status.',
            '%d outings were set to DRAFT status.',
            updated,
        ) % updated, messages.SUCCESS)

    def move_to_CANCELED_status(self, request, queryset):
        status = Status.objects.filter(name='CANCELED')[0]
        updated = queryset.update(status=status)
        self.message_user(request, ngettext(
            '%d outing was set to CANCELED.',
            '%d outings were set to CANCELED',
            updated,
        ) % updated, messages.SUCCESS)

    def subscriptions_open(self, request, queryset):
        updated = queryset.update(open_sub=True)
        self.message_user(request, ngettext(
            '%d subscription open.',
            '%d subscription were open to public.',
            updated,
        ) % updated, messages.SUCCESS)

    def subscriptions_close(self, request, queryset):
        updated = queryset.update(open_sub=False)
        self.message_user(request, ngettext(
            '%d subscription closed.',
            '%d subscriptions were closed to public.',
            updated,
        ) % updated, messages.SUCCESS)

    model = Outing
    list_display = [
        'place',
        'start_date',
        'official',
        'open_sub',
        'status',
        'max_participants',
        'season',
        'gps_file_name',
        'outing_file_name',
    ]
    inlines = [
        OutingTeamInline,
        ]
    list_filter = ('season', 'open_sub', 'official', 'status', 'place__type', 'place__massif__country', 'place__massif__name')
    list_per_page = 250
    list_max_show_all = 1000
    search_fields = ('place__name', 'place__massif__name')
    actions = (move_to_OK_status, move_to_DRAFT_status, move_to_CANCELED_status, subscriptions_open, subscriptions_close, set_five_participants, set_COVID_related_note)
    ordering = ( '-start_date',)


@admin.register(OutingTeam)
class OutingTeamAdmin(admin.ModelAdmin):

    def move_to_OK_status(self, request, queryset):
        status = Status.objects.filter(name='OK')[0]
        updated = queryset.update(status=status)
        self.message_user(request, ngettext(
            '%d outing was set to OK status.',
            '%d outings were set to OK status',
            updated,
        ) % updated, messages.SUCCESS)

    def move_to_WAITINGLIST_status(self, request, queryset):
        status = Status.objects.filter(name='DRAFT')[0]
        updated = queryset.update(status=status)
        self.message_user(request, ngettext(
            '%d outing was set to DRAFT status.',
            '%d outings were set to DRAFT status.',
            updated,
        ) % updated, messages.SUCCESS)

    def move_to_CANCELED_status(self, request, queryset):
        status = Status.objects.filter(name='CANCELED')[0]
        updated = queryset.update(status=status)
        self.message_user(request, ngettext(
            '%d outing was set to CANCELED.',
            '%d outings were set to CANCELED',
            updated,
        ) % updated, messages.SUCCESS)

    def move_to_POSTOPNED_status(self, request, queryset):
        status = Status.objects.filter(name='POSTPONED')[0]
        updated = queryset.update(status=status)
        self.message_user(request, ngettext(
            '%d outing was set to POSTPONED.',
            '%d outings were set to POSTPONED',
            updated,
        ) % updated, messages.SUCCESS)

    model = OutingTeam
    list_display = [
        'member',
        'outing',
        'status',
        'present',
        'url_photo',
        'date_added',
        'date_modified',
        'mobile',
    ]
    list_per_page = 250
    list_max_show_all = 1000
    ordering = ( '-date_added',)
    list_filter = ('outing__season', 'outing__status', 'outing__place__massif__country', 'outing__official')
    search_fields = ('outing__place__name',)
    actions = (move_to_OK_status, move_to_WAITINGLIST_status, move_to_POSTOPNED_status, move_to_CANCELED_status)


@admin.register(WebMessage)
class WebMessageAdmin(admin.ModelAdmin):
    model = WebMessage
    list_display = ['valid', 'mainPage', 'outingPage', 'description' ]
    fields = ['valid', 'mainPage', 'outingPage', 'description']