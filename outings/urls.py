from django.urls import path

from . import views
from . import ajax

app_name = 'skiclubtouring'
urlpatterns = [
    path('', views.index, name='index'),
    path('outings', views.outings),
    path('outing/new', views.outing_new, name='outing_new'),
    path('outing/<int:oid>', views.details, name='outing'),
    path('outing/<int:oid>/subscribe',views.subscribe, name='subscribe'),
    path('outing/<int:oid>/list', views.members, name='outing_list'),
    path('outing/<int:oid>/toggle', views.outing_toggle, name='outing_toggle'),
    path('outing/<int:oid>/remove', views.outing_remove, name='outing_remove'),
    path('outing/<int:oid>/status', views.outing_status, name='outing_status'),
    path('details', views.details,),  # TODO remove at some point
    path('members', views.members),  # TODO remove at some point
    path('subscribe', views.subscribe),  # TODO remove at some point
    path('map', views.map),
    path('map-all', views.map_all),
    path('photos', views.photos),
    path('photospublic', views.photos_simple),
    path('member', views.member_outing, name="member"),
    path('member/instructor', views.season_summary, name="instructor"),
    path('stats', views.stats, name="stats"),
    path('export-draft', views.export),
    path('ajax/outingsof/<int:mid>', ajax.outings),
    path('ajax/stats/global', ajax.stats_global),
    path('ajax/stats/global/season/<int:sid>', ajax.stats_season),
    path('ajax/stats/user/outings', ajax.stats_outings),
    path('ajax/stats/user/regions', ajax.stats_regions)
]
