from datetime import datetime

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.functional import cached_property

from members.models import Member, Season
from places.models import Place


class Status(models.Model):
    name = models.CharField(max_length=9, default=None, unique=True)
    importance = models.IntegerField(default=99)

    def __str__(self):
        return f'{self.name}'


class Outing(models.Model):
    class Meta:
        ordering = ['season']

    outing_id = models.AutoField(primary_key=True, serialize=False,
                                 verbose_name='outing_id')
    start_date = models.DateField(blank=True, null=True, default=datetime.now)
    end_date = models.DateField(blank=True, null=True, default=datetime.now)
    season = models.ForeignKey(Season, blank=True, null=True,
                               on_delete=models.SET_NULL)
    place = models.ForeignKey(Place, blank=True, null=True,
                              on_delete=models.SET_NULL)

    open_sub = models.BooleanField(_('Is opened for subscriptions'), default=True)
    official = models.BooleanField(_('official'), default=True)
    status = models.ForeignKey(Status, blank=True, null=True,
                               on_delete=models.SET_NULL)
    max_participants = models.IntegerField(null=True, blank=True, default=5)

    details = models.TextField(max_length=2000, blank=True, default=None, null=True)
    summary = models.TextField(max_length=8000, blank=True, default=None, null=True)
    note = models.TextField(max_length=2000, blank=True, default=None, null=True)

    gps_file_name = models.CharField(max_length=40, blank=True, default=None, null=True)
    outing_file_name = models.CharField(max_length=40, blank=True, default=None, null=True)

    created = models.DateTimeField(blank=True, null=True, default=datetime.now)
    modified = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return f'{self.place} on {self.start_date}'

    @property
    def day_of_week(self):
        import calendar
        return calendar.day_name[self.start_date.weekday()]

    @property
    def is_past_due(self):
        return datetime.now().date() > self.start_date


Outing._meta.get_field('official').help_text = _('Available for all members.')
Outing._meta.get_field('details').help_text = _('Description of the outing')
Outing._meta.get_field('note').help_text = _('Warning message (displayed in red)')
Outing._meta.get_field('gps_file_name').help_text = _('Link to the publicly accessible file')
Outing._meta.get_field('outing_file_name').help_text = _('Link to the publicly accessible file with the scanned form')


class OutingTeam(models.Model):
    class Meta:
        ordering = ['date_added']

    member = models.ForeignKey(Member, blank=True, null=True,
                               on_delete=models.SET_NULL)
    organiser = models.BooleanField(_('organiser'), default=False)
    outing = models.ForeignKey(Outing, blank=True, null=True,
                               on_delete=models.SET_NULL)

    status = models.ForeignKey(Status, blank=True, null=True,
                               on_delete=models.SET_NULL, default='OK', to_field='name',
                               help_text=_('Set DRAFT if you wish be on the WAITING LIST, otherwise leave OK'))

    contact_email = models.EmailField(_('New email address'), blank=True, null=True,
                                      help_text=_('Set if other than the one registered'))
    contact_mobile = models.CharField(_('New phone contact '), max_length=13, blank=True,
                                      default=None, null=True,
                                      help_text=_('Set if other than the one registered'))
    comments = models.TextField(max_length=1000, blank=True, default=None, null=True)
    dva = models.BooleanField(_('DVA'), default=True,
                              help_text=_('(Uncheck it if you need one from club)'))
    shovel = models.BooleanField(_('Shovel'), default=True,
                                 help_text=_('(Uncheck it if you need one from club)'))
    probe = models.BooleanField(_('Probe'), default=True,
                                help_text=_('(Uncheck it if you need one from club)'))

    url_photo = models.URLField(blank=True, default=None, null=True)
    url_video = models.URLField(blank=True, default=None, null=True)

    present = models.BooleanField(_('Present'), default=True,
                                help_text=_('Uncheck this if not present'))

    date_added = models.DateTimeField(blank=True, null=True, default=datetime.now)
    date_modified = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return f'{self.member.first_name} for {self.outing.place.name} on {self.outing.start_date}'

    @cached_property
    def email(self):
        if self.contact_email is not None:
            return self.contact_email
        else:
            return self.member.email

    @cached_property
    def mobile(self):
        if self.contact_mobile is not None:
            return self.contact_mobile
        else:
            return self.member.mobile


class WebMessage(models.Model):

    number = models.AutoField(primary_key=True, blank=True)
    description = models.TextField()
    valid = models.BooleanField(default=False)
    mainPage = models.BooleanField(default=True)
    outingPage = models.BooleanField(default=True)