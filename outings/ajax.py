from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse
from django.db.models import Q
from django.db.models import Count, Sum
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from django.contrib.messages import get_messages
from django.contrib import messages

from django.views.decorators.clickjacking import xframe_options_exempt

from django.template import loader
from django.template.defaulttags import register

from outings.models import *
from outings.views import establish_variables
from outings.forms import *
from members.models import *
from places.models import *


@login_required
def outings(request, mid=None):
    scheduled_members_outings = OutingTeam.objects.filter(member__member_id=mid). \
                                    filter(outing__isnull=False). \
                                    order_by("-outing__start_date")[:15]

    return JsonResponse({one.outing.outing_id: {'outing': one.outing.place.name,
                                                'outing_status': one.outing.status.name,
                                                'status': one.status.name,
                                                'date': one.outing.start_date,
                                                } for one in scheduled_members_outings})


@login_required
def stats_global(request):
    membersDict = {}
    members = Member.objects.all()
    for one in members:
        membersDict[one.member_id] = one

    countsTotal = OutingTeam.objects.filter(outing__status__name='OK', status__name='OK'). \
                      values('member').annotate(total=Count('member'),
                                                totalDiff=Sum('outing__place__cumulated_diff')). \
                      order_by('-total')[:20]

    countsTotalProper = []
    diffTotalProper = []
    categories = []
    for one in countsTotal:
        categories.append(membersDict[one['member']].first_name + " " + membersDict[one['member']].last_name)
        countsTotalProper.append(one['total'])
        diffTotalProper.append(one['totalDiff'] / 1000)

    return JsonResponse({"top": {"who": categories, "count": countsTotalProper, "diff": diffTotalProper}})


@login_required
def stats_season(request, sid=None):
    # nbMemberOutings = len(OutingTeam.objects.filter(outing__status__name='OK', status__name='OK'))
    # nbOutings = len(Outing.objects.filter(status__name='OK'))
    # general = {'nbOutings': nbOutings, 'nbMemberOutings': nbMemberOutings}
    membersDict = {}
    members = Member.objects.all()
    for one in members:
        membersDict[one.member_id] = one
    season, messagesStart = establish_variables(request)
    if sid is not None:
        season = Season.objects.get(start=sid)
    countsSeason = OutingTeam.objects.filter(outing__season=season, outing__status__name='OK',
                                             status__name='OK',
                                             member__instructor=False). \
        values('member').annotate(total=Count('member'),
                                  totalDiff=Sum('outing__place__cumulated_diff')). \
        order_by('-total')
    countsSeasonProper = []
    for one in countsSeason:
        countsSeasonProper.append(
            {'member': membersDict[one['member']].first_name, 'total': one['total'], 'totalDiff': one['totalDiff']})

    countsTotalProper = []
    diffTotalProper = []
    categories = []
    for one in countsSeason:
        categories.append(membersDict[one['member']].first_name + " " + membersDict[one['member']].last_name)
        countsTotalProper.append(one['total'])
        diffTotalProper.append(one['totalDiff'] / 1000)

    return JsonResponse({"season": {"who": categories, "count": countsTotalProper, "diff": diffTotalProper}})


@login_required
def stats_outings(request):
    member = request.user
    baseQ = OutingTeam.objects.filter(member=member, outing__status__name='OK', status__name='OK')
    scheduled_outings_figures = baseQ.values('outing__season__name').order_by().annotate(Count('outing__season'))
    year = [one['outing__season__name'] for one in scheduled_outings_figures],
    outings = [one['outing__season__count'] for one in scheduled_outings_figures]
    return JsonResponse({"years": year[0], "outings": outings})


@login_required
def stats_regions(request):
    member = request.user
    baseQ = OutingTeam.objects.filter(member=member, outing__status__name='OK', status__name='OK')
    regions = baseQ.values('outing__place__massif__name').order_by().annotate(Count('outing__place__massif'))
    labels = [one['outing__place__massif__name'] for one in regions]
    values = [one['outing__place__massif__count'] for one in regions]
    return JsonResponse({"data": [{"name": one[0], "y": one[1]} for one in zip(labels, values)]})
