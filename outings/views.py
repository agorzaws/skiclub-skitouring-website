from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.db.models import Q
from django.db.models import Count, Sum
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from django.contrib.messages import get_messages
from django.contrib import messages

from django.views.decorators.clickjacking import xframe_options_exempt

from django.template import loader
from django.template.defaulttags import register
from django.views.decorators.csrf import csrf_protect

from outings.forms import *
from members.models import *
from places.models import *

from skiclubtouring.settings import MAIN_PAGE_HOME_BUTTON, PUBLIC_EMAIL, WEB_APP_AUTHOR

import datetime


class OneDetails:
    # TODO maybe move it to some external utility classes
    def __init__(self, totalOutings=0, currentSeasonOutings=0, cancelledMemberOutings=0, secuDay=False):
        self.totalOutings = totalOutings
        self.currentSeasonOutings = currentSeasonOutings
        self.cancelledMemberOutings = cancelledMemberOutings
        self.secuDay = secuDay
        self.badge = 'danger'
        if totalOutings > 10:
            self.badge = 'warning'
        if totalOutings > 30:
            self.badge = 'success'


def getLoggedMember(request):
    logged_user_first_name = 'Stranger, try to login'
    logged_user = False
    if request.user.is_authenticated:
        logged_user_first_name = request.user.first_name
        logged_user = True
    return logged_user, logged_user_first_name


def establish_variables(request):
    # TODO fix this function as I have no idea what I wanted to achieve///
    req_season = request.GET.get("season")
    err = None
    season = None
    try:
        season_year = int(req_season)
        season = Season.objects.filter(end=season_year).first()
    except (ValueError, TypeError):
        pass
    if season is None:
        season = Season.objects.order_by("-end").first()
        if season is None:
            return None, ["No seasons found."]
        if req_season:
            err = "No season like '{}' found. Switching to the last one in the calendar: {}".format(
                req_season, season
            )
    return (
        season,
        [err],
    )


def find_future_active_WE():
    saturday, sunday = find_future_active_WE_full()
    return [saturday.strftime("%Y-%m-%d"), sunday.strftime("%Y-%m-%d"), int(saturday.strftime("%Y"))]


def find_future_active_WE_full():
    SATURDAY = 5
    SUNDAY = 6
    today = datetime.date.today()
    if today.weekday() in [SATURDAY, SUNDAY]:
        if today.weekday() == SATURDAY:
            return today, today + datetime.timedelta(1)
        else:
            return today - datetime.timedelta(1), today
    else:
        saturday = today + datetime.timedelta((SATURDAY - today.weekday()) % 7)
        sunday = today + datetime.timedelta((SUNDAY - today.weekday()) % 7)
        return saturday, sunday


def static_context_and_add(request, toAdd={}):
    toReturn = {'activeWE': find_future_active_WE(),
                'messages': get_messages(request),
                'webPageHome': MAIN_PAGE_HOME_BUTTON,
                'publicContactEmail': PUBLIC_EMAIL,
                'webAppAuthor': WEB_APP_AUTHOR,
                'smallListView': False,
                }
    for key, item in toAdd.items():
        toReturn[key] = item
    return toReturn


def login_view(request, context={}):
    if request.method == 'POST':
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            user = authenticate(
                username=login_form.cleaned_data['username'].lower(),
                password=login_form.cleaned_data['password']
            )
            if user is not None:
                login(request, user)
            else:
                messages.error(request, 'Username/password incorrect')
            return redirect('/')
        else:
            messages.error(request, 'The form you submitted could not be validated, please try again')
    else:
        login_form = LoginForm({})
    context['login_form'] = login_form
    return render(request=request,
                  template_name='login.html',
                  context=context)


def logout_view(request):
    logout(request)
    return render(request=request,
                  template_name='outings.html')


def prepareErrorPage(request, alert):
    context = {'errorMessage': alert, }
    return render(request, 'error.html', context)


def generate_main_context_for_outings_list(request):
    active_season, alertMessages = establish_variables(request)
    web_messages = WebMessage.objects.filter(valid=True)
    for one in web_messages:
        alertMessages.append(one.description)
    logged_user, logged_user_first_name = getLoggedMember(request)

    seasons = Season.objects.all().order_by('-end')
    scheduled_outings = Outing.objects.filter(season=active_season).filter(~Q(status__name='DRAFT')).order_by(
        'start_date', 'created')
    scheduled_outings_length = len(Outing.objects.filter(season=active_season).filter(status__name='OK').order_by(
        'start_date', 'created'))
    instructor_logged = False
    if logged_user and request.user.instructor:
        scheduled_outings = Outing.objects.filter(season=active_season).order_by('start_date', 'created')
        instructor_logged = True
    scheduled_outings_all = {}
    scheduled_outings_subscribe = {}
    for one in scheduled_outings:
        outing_instructors = OutingTeam.objects.filter(outing=one, member__instructor=True)
        scheduled_outings_all[one] = outing_instructors
        if one.open_sub:
            scheduled_outings_subscribe[one] = outing_instructors
    context = static_context_and_add(request, toAdd={'scheduled_outings': scheduled_outings,
                                                     'scheduled_outings_all': scheduled_outings_all,
                                                     'scheduled_outings_length': scheduled_outings_length,
                                                     'scheduled_outings_subscribe': scheduled_outings_subscribe,
                                                     'seasons': seasons,
                                                     'logged_user_first_name': logged_user_first_name,
                                                     'logged_user': logged_user,
                                                     'alertMessages': alertMessages,
                                                     'active_season': active_season,
                                                     'innerClub': False,
                                                     'instructor_logged': instructor_logged})
    return context


def index(request):
    context = generate_main_context_for_outings_list(request)
    return render(request, 'outings.html', context)


@xframe_options_exempt
def outings(request):
    context = generate_main_context_for_outings_list(request)
    context['smallListView'] = True  # override the one from the basic context
    return render(request, 'outings-simple.html', context)


def generate_main_context_for_photos_list(request):
    active_season, alertMessages = establish_variables(request)
    logged_user, logged_user_first_name = getLoggedMember(request)

    seasons = Season.objects.all().order_by('-end')
    scheduled_outings = Outing.objects.filter(season=active_season).order_by('-start_date')
    scheduled_outings_all = {}
    for one in scheduled_outings:
        outing_instructors = OutingTeam.objects.filter(outing=one, member__instructor=True)
        outing_photos = OutingTeam.objects.filter(outing=one).filter(~Q(url_photo=None))

        if len(outing_photos):
            scheduled_outings_all[one] = (outing_instructors, outing_photos)

    context = static_context_and_add(request, toAdd={'logged_user_first_name': logged_user_first_name,
                                                     'logged_user': logged_user,
                                                     'scheduled_outings_all': scheduled_outings_all,
                                                     'seasons': seasons,
                                                     'alertMessages': alertMessages,
                                                     'active_season': active_season,
                                                     'innerClub': False,
                                                     })
    return context


def photos(request):
    context = generate_main_context_for_photos_list(request)
    return render(request, 'photos.html', context)


@xframe_options_exempt
def photos_simple(request):
    context = generate_main_context_for_photos_list(request)
    context['smallListView'] = True  # override the one from the basic context
    return render(request, 'photos-simple.html', context)


def map(request):
    active_season, alertMessages = establish_variables(request)
    seasons = Season.objects.all().order_by('-end')
    scheduled_outings = Outing.objects.filter(season=active_season, status__name='OK').order_by('start_date')
    context = static_context_and_add(request, toAdd={'scheduled_outings': scheduled_outings,
                                                     'seasons': seasons,
                                                     'alertMessages': alertMessages,
                                                     'innerClub': False,
                                                     'active_season': active_season,
                                                     })
    return render(request, 'map-new.html', context)


def map_all(request):
    places_outings = {}
    for place in Place.objects.all():
        outings = Outing.objects.filter(place=place, status__name='OK').order_by('-start_date')
        if len(outings):
            places_outings[place] = outings
            # TODO fix the mapping for the places with the same GPS

    context = static_context_and_add(request, toAdd={'places_outings': places_outings})
    return render(request, 'map-all.html', context)


def details(request, oid=None):
    active_season, alertMessages = establish_variables(request)
    web_messages = WebMessage.objects.filter(valid=True, outingPage=True)
    for one in web_messages:
        alertMessages.append(one.description)
    logged_user, logged_user_first_name = getLoggedMember(request)
    instructor_logged = False
    if logged_user and request.user.instructor:
        instructor_logged = True

    outing_id = oid
    try:
        if outing_id is None:
            outing_id = forms.IntegerField().clean(request.GET.get('outing_id'))
    except ValidationError as e:
        return prepareErrorPage(request, '!!outing_id manipulated!! {}'.format(e))

    concernedOutings = Outing.objects.filter(outing_id=outing_id)
    if len(concernedOutings) != 1:
        return prepareErrorPage(request,
                                'Some manipulation on the outing_id detected. Next time, choose your path wiser!')

    outing = concernedOutings[0]

    previousOutings = Outing.objects.filter(place=outing.place).order_by('-start_date')

    outing_members = OutingTeam.objects.filter(outing=outing).filter(~Q(status__name='CANCELED')).order_by(
        'status__importance', 'date_added', 'member__last_name')
    instructors, participants = getOutingTeamStats(active_season, outing_members)

    context = static_context_and_add(request, toAdd={'alertMessages': alertMessages,
                                                     'active_season': active_season,
                                                     'outing': outing,
                                                     'previousOutings': previousOutings,
                                                     'outing_members': outing_members,
                                                     'instructors': instructors,
                                                     'participants': participants,
                                                     'innerClub': False,
                                                     'instructor_logged': instructor_logged,
                                                     })
    return render(request, 'details.html', context)


def getOutingTeamStats(active_season, outing_members):
    instructors = []
    participants = []
    for one in outing_members:
        if one.organiser:
            instructors.append(one)
        else:
            totalOutings = len(
                OutingTeam.objects.filter(member=one.member, outing__status__name='OK', status__name='OK'))

            currentSeasonOutingsQ = (OutingTeam.objects.filter(member=one.member,outing__season__name=active_season)
                                     .filter(Q(outing__status__name='POSTPONED') | Q( outing__status__name='OK')))
            currentSeasonOutings = len(currentSeasonOutingsQ.filter(status__name='OK'))
            cancelledMemberOutings = len(
                currentSeasonOutingsQ.filter(Q(status__name='POSTPONED') | Q(status__name='DRAFT')))
            presentInSecuDay = len(currentSeasonOutingsQ.filter(outing__place__secu_day=True)) > 0

            oneDetails = OneDetails(totalOutings=totalOutings, currentSeasonOutings=currentSeasonOutings,
                                    cancelledMemberOutings=cancelledMemberOutings, secuDay=presentInSecuDay)
            participants.append((one, oneDetails))
    return instructors, participants


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@login_required
def season_summary(request):
    member = request.user
    if not member.is_staff:
        return prepareErrorPage(request, 'You are not allowed to access that page!')

    season, alertMessagesToSkip = establish_variables(request)

    # instructors end-of-season overview
    instructors = Member.objects.filter(instructor=True, active=True)
    instructors_outing = {}
    for instructor in instructors:
        outingTeams = OutingTeam.objects.filter(outing__season=season,
                                                outing__status__name="OK",
                                                member=instructor).order_by('outing__start_date')
        instructors_outing[instructor.member_id] = [outingTeam.outing for outingTeam in outingTeams]

    # active members stats
    active_members = Member.objects.filter(season=season).order_by('-instructor', 'last_name')
    active_members_outings = {}
    active_members_outings_all = {}
    for m in active_members:
        x = len(OutingTeam.objects.filter(outing__season=season,
                                          status__name="OK",
                                          outing__status__name="OK",
                                          outing__place__secu_day=False,
                                          member=m))
        if x:
            active_members_outings[m.member_id] = x
            active_members_outings_all[m.member_id] = len(OutingTeam.objects.filter(
                outing__status__name="OK",
                member=m))

    # upcoming weekends stats
    a = find_future_active_WE()
    next_weekend_outing = Outing.objects.filter(Q(start_date=a[0]) | Q(start_date=a[1])). \
        order_by('start_date')
    outings_details = {}
    for one in next_weekend_outing:
        next_weekend_team = OutingTeam.objects.filter(outing=one)
        instructorsX, participants = getOutingTeamStats(season, next_weekend_team)
        outings_details[one.outing_id] = participants

    draftOutings = Outing.objects.filter(season=season, status__name="DRAFT")
    outings_open = Outing.objects.filter(open_sub=True)
    context = static_context_and_add(request, toAdd={
        'member': member,
        'alertMessages': alertMessagesToSkip,
        'innerClub': True,
        'season': season,
        'statuses': Status.objects.all(),
        'draftOutings': draftOutings,
        'instructors': instructors,
        'instructors_outing': instructors_outing,
        'instructor_logged': member.instructor,
        'active_members': active_members,
        'active_members_outings': active_members_outings,
        'active_members_outings_all': active_members_outings_all,
        'next_weekend_outing': next_weekend_outing,
        'next_weekend_details': outings_details,
        'open_subscriptions_outings': outings_open,
    })
    return render(request, 'season_summary.html', context)


@login_required
def stats(request):
    member = request.user
    nbMemberOutings = len(OutingTeam.objects.filter(outing__status__name='OK', status__name='OK'))
    nbOutings = len(Outing.objects.filter(status__name='OK'))
    general = {'nbOutings': nbOutings, 'nbMemberOutings': nbMemberOutings}
    membersDict = {}
    members = Member.objects.all()
    for one in members:
        membersDict[one.member_id] = one
    firstSeason = Season.objects.all().order_by('end')[0]
    season, messagesStart = establish_variables(request)

    countsSeason = OutingTeam.objects.filter(outing__season=season, outing__status__name='OK',
                                             status__name='OK',
                                             member__instructor=False). \
        values('member').annotate(total=Count('member'),
                                  totalDiff=Sum('outing__place__cumulated_diff')). \
        order_by('-total')
    countsSeasonProper = []
    for one in countsSeason:
        countsSeasonProper.append(
            {'member': membersDict[one['member']], 'total': one['total'], 'totalDiff': one['totalDiff']})

    countsTotal = OutingTeam.objects.filter(outing__status__name='OK', status__name='OK'). \
                      values('member').annotate(total=Count('member'),
                                                totalDiff=Sum('outing__place__cumulated_diff')). \
                      order_by('-total')[:20]
    countsTotalProper = []
    for one in countsTotal:
        countsTotalProper.append(
            {'member': membersDict[one['member']], 'total': one['total'], 'totalDiff': one['totalDiff']})

    context = static_context_and_add(request, toAdd={'firstSeason': firstSeason,
                                                     'member': member,
                                                     'general': general,
                                                     'alertMessages': messagesStart,
                                                     'innerClub': True,
                                                     'season': season,
                                                     'seasons': Season.objects.all().order_by('-end'),
                                                     'top10season': countsSeasonProper[:10],
                                                     'countSeason': len(countsSeasonProper),
                                                     'top10total': countsTotalProper[:10],
                                                     'instructor_logged': member.instructor,
                                                     })
    return render(request, 'member_stats.html', context)


@login_required
def member_outing(request):
    member = request.user
    baseQ = OutingTeam.objects.filter(member=member, outing__status__name='OK', status__name='OK')
    scheduled_members_outings = baseQ.order_by('-outing__start_date')

    context = static_context_and_add(request, toAdd={'member': member,
                                                     'scheduled_members_outings': scheduled_members_outings,
                                                     'innerClub': True,
                                                     'instructor_logged': member.instructor,
                                                     })
    return render(request, 'member_outings.html', context)


@login_required
def members(request, oid=None):
    active_season, alertMessages = establish_variables(request)
    outing_id = -1
    try:
        outing_id = oid
        if outing_id is None:
            outing_id = forms.IntegerField().clean(request.GET.get('outing_id'))
    except ValidationError as e:
        return prepareErrorPage(request, '!!outing_id manipulated!! {}'.format(e))

    member = request.user
    outing = Outing.objects.filter(outing_id=outing_id)[0]

    outing_members = OutingTeam.objects.filter(outing=outing).filter(~Q(status__name='CANCELED')).order_by(
        'status__importance', 'date_added', 'member__last_name')
    instructors, participants = getOutingTeamStats(active_season, outing_members)

    context = static_context_and_add(request, toAdd={'member': member,
                                                     'outing': outing,
                                                     'innerClub': True,
                                                     'instructors': instructors,
                                                     'participants': participants,
                                                     'instructor_logged': member.instructor
                                                     })
    return render(request, 'members_info.html', context)


@csrf_protect
@login_required
def outing_new(request):
    active_season, alertMessages = establish_variables(request)

    if request.method == "POST":
        form = OutingForm(request.POST)

        if form.is_valid():
            outing = form.save(commit=False)
            outing.season = active_season
            outing.end_date = outing.start_date
            outing.status = Status.objects.get(name="DRAFT")
            outing.open_sub = False
            outing.save()
            ot = OutingTeam(member=request.user, outing=outing)
            ot.organiser = True
            ot.save()
            message = _("You have created outing {}, for now it is in mode DRAFT".format(outing))
            messages.success(request, message)
        else:
            message = _("You have not added new outing due {}".format(form.errors.as_data()))
            messages.error(request, message)
        return HttpResponseRedirect(reverse("skiclubtouring:instructor"))

    context = static_context_and_add(request, toAdd={
        'form': OutingForm(),
        'innerClub': True,
        'member': request.user,
        'instructor_logged': request.user.instructor
    })
    return render(request, 'outings-add.html', context)


@login_required
def subscribe(request, oid=None):
    if not request.user.is_authenticated:
        return login_view(request)

    outing_id = -1
    try:
        outing_id = oid
        if outing_id is None:
            outing_id = forms.IntegerField().clean(request.GET.get('outing_id'))
    except ValidationError as e:
        return prepareErrorPage(request, '!!outing_id manipulated!! {}'.format(e))

    currentOutings = Outing.objects.filter(outing_id=outing_id)
    if len(currentOutings) != 1:
        return prepareErrorPage(request, 'Some manipulation on the outing_id detected, subscription CANCELED!')
    outing = currentOutings[0]

    if not outing.open_sub:
        return prepareErrorPage(request, 'You are trying to subscribe to the outing that is NOT open for subscription.\
                If problem persists, please contact our team with email given in the footer.')

    if not request.user.active or request.user.season != outing.season:
        return prepareErrorPage(request, 'You are trying to subscribe to the outing while you are NOT AN active member\
                                            of the Skiclub SkiTouring section. If problem persists, please contact our\
                                            team with email given in the footer of this web page.')

    subscribed = False
    if request.method == "POST":
        # TODO extract that as a separate endpoint
        form = OutingMemberForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            extraText = ""
            saturday, sunday = find_future_active_WE_full()
            nbOfOutingsTheSameWE = len(OutingTeam.objects \
                                       .filter(Q(member_id=request.user.member_id) & ~Q(status__name='DRAFT'))
                                       .filter(Q(outing__start_date=saturday) | Q(outing__start_date=sunday))) > 1
            if nbOfOutingsTheSameWE:
                post.status = Status.objects.filter(name='DRAFT')[0]
                extraText = ' as you already subscribed to another outing this WE!. We hope you understand that.\
                            Nonetheless, you will be contacted at the same time along with the others.'

            numberOfSubscribedMembersSoFar = len(
                OutingTeam.objects.filter(outing_id=outing.outing_id, status__name='OK'))
            if outing.max_participants is not None and numberOfSubscribedMembersSoFar > outing.max_participants:
                post.status = Status.objects.filter(name='DRAFT')[0]
                extraText = ' as you subscribed above the maximum limit! \
                            Nonetheless, you will be contacted at the same time when the others.'

            post.date_added = timezone.now()
            post.member = request.user
            post.outing = outing
            post.url_photo = None
            post.url_video = None
            if len(OutingTeam.objects.filter(member=request.user, outing=outing)) == 0:
                subscribed = True
                post.save()
                message = _("{}, you are added to the list for {} with status {} {}"
                            .format(request.user.first_name, outing.place.name, post.status, extraText))
                messages.success(request, message)
            else:
                message = _("{}, you are already subscribed, skipping".format(request.user.first_name))
                messages.warning(request, message)

            return HttpResponseRedirect(reverse("skiclubtouring:index"))

    outing_members = OutingTeam.objects.filter(outing=outing).order_by('date_added', 'member__last_name')
    instructors = []
    participants = []
    for one in outing_members:
        if one.organiser:
            instructors.append(one)
        else:
            if request.user == one.member:
                subscribed = True
            participants.append(one)
    alertMessages = []
    web_messages = WebMessage.objects.filter(valid=True, outingPage=True)
    for one in web_messages:
        alertMessages.append(one.description)
    context = static_context_and_add(request, toAdd={'innerClub': True,
                                                     'subscribed': subscribed,
                                                     'member': request.user,
                                                     'outing': outing,
                                                     'outing_members': outing_members,
                                                     'instructors': instructors,
                                                     'participants': participants,
                                                     'form': OutingMemberForm(),
                                                     'alertMessages': alertMessages,
                                                     'instructor_logged': request.user.instructor
                                                     })
    return render(request, 'subscribe.html', context)


@login_required
def export(request):
    active_season, alertMessages = establish_variables(request)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="season{}_DRAFT.csv"'.format(active_season.end)
    scheduled_outings = Outing.objects.filter(season=active_season, status__name='DRAFT').order_by('start_date',
                                                                                                   'created')
    t = loader.get_template('outing-export.txt')
    c = {'outings_to_export': scheduled_outings}
    response.write(t.render(c))
    return response


@login_required
def outing_remove(request, oid=None):
    if request.user.is_staff:
        outing = Outing.objects.get(outing_id=oid)
        if outing.status.name == "DRAFT":
            outingStr = outing.__str__()
            x = OutingTeam.objects.filter(outing=outing)
            for one in x:
                one.delete()
            outing.delete()
            message = _("You have deleted DRAFT outing {}, no way back...".format(outingStr))
            messages.success(request, message)
    return HttpResponseRedirect(reverse("skiclubtouring:instructor"))


@login_required
def outing_toggle(request, oid=None):
    if request.user.is_staff:
        outing = Outing.objects.get(outing_id=oid)
        open = outing.open_sub
        outing.open_sub = not open
        outing.save()
        message = _("You have changed the subscription status {}".format(outing))
        messages.success(request, message)
    return HttpResponseRedirect(reverse("skiclubtouring:instructor"))


@login_required
@csrf_protect
def outing_status(request, oid=None):
    new_status = Status.objects.get(name="OK")
    if request.POST:
        new_status = Status.objects.get(name=request.POST['status'])

    if request.user.is_staff:
        outing = Outing.objects.get(outing_id=oid)
        old_status = outing.status
        outing.status = new_status
        outing.save()
        message = _("You have changed the outing status {}->{} {}".
                    format(old_status, new_status, outing))
        messages.success(request, message)
    return HttpResponseRedirect(reverse("skiclubtouring:instructor"))
