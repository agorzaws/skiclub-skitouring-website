from django import forms
from django.forms import ValidationError

from .models import *

from members.models import Member
from outings.models import OutingTeam

import calendar
import datetime


class LoginForm(forms.Form):
    username = forms.CharField(max_length=25)
    username.widget.attrs = {'size': 8}
    password = forms.CharField(max_length=30, widget=forms.PasswordInput)
    password.widget.attrs = {'size': 8}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class OutingMemberForm(forms.ModelForm):
    class Meta:
        model = OutingTeam
        fields = ('status', 'contact_email', 'contact_mobile', 'comments', 'dva', 'probe', 'shovel')


class OutingForm(forms.ModelForm):
    class Meta:
        model = Outing
        fields = ('official', 'place', 'start_date',
                  'max_participants', 'details', 'note')
