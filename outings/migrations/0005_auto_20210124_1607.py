# Generated by Django 3.1.3 on 2021-01-24 15:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('outings', '0004_auto_20210117_2225'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='name',
            field=models.CharField(default=None, max_length=9, unique=True),
        ),
        migrations.AddField(
            model_name='outingteam',
            name='status',
            field=models.ForeignKey(blank=True, default='OK', null=True, on_delete=django.db.models.deletion.SET_NULL, to='outings.status', to_field='name'),
        ),
    ]
