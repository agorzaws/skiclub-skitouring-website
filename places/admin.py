from django.contrib import admin, messages
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.conf import settings
from django.db.models import Q, Max

from .models import *


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    model = Country
    list_display = [
        'name',
        'code'
    ]


@admin.register(Massif)
class MassifAdmin(admin.ModelAdmin):
    model = Massif
    list_display = [
        # 'massif_id',
        'name',
        'description',
        'country',
        'avalanche',
        'created',
        'modified'
    ]
    list_filter = ('country',)
    list_per_page = 250
    list_max_show_all = 1000
    ordering = ('name', 'country')
    # readonly_fields = ('massif_id',)


@admin.register(PlaceType)
class PlaceTypeAdmin(admin.ModelAdmin):
    model = PlaceType
    list_display = [
        'name',
    ]


@admin.register(SkiGrade)
class SkiGradeAdmin(admin.ModelAdmin):
    model = SkiGrade
    list_display = [
        'name',
        'description'
    ]


@admin.register(SlopeGrade)
class SlopeGradeAdmin(admin.ModelAdmin):
    model = SlopeGrade
    list_display = [
        'name',
        'description'
    ]


@admin.register(SlopeAspect)
class SlopeAspectAdmin(admin.ModelAdmin):
    model = SlopeAspect
    list_display = [
        'name',
        'description'
    ]


@admin.register(SkiLevel)
class SkiLevelAdmin(admin.ModelAdmin):
    model = SkiLevel
    list_display = [
        'name',
        'description'
    ]


@admin.register(SkiTourerPack)
class SkiTourerPackAdmin(admin.ModelAdmin):
    model = SkiTourerPack
    list_display = [
        'name',
        'comments'
    ]


@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
    model = Place
    list_display = [
        'name',
        # 'description',
        'ski_level',
        'ski_grade',
        'slope_grade',
        'aspect',
        'type',
        'massif',
        'ski_pack'
    ]
    fields = (
        'name',
        'description',
        'massif',
        'secu_day',
        ('alt_start', 'alt_summit', 'cumulated_diff'),
        ('lat', 'long'),
        ('lat_wsg84', 'long_wsg84'),

        'nb_days',
        ('ski_level',
         'ski_grade',
         'slope_grade'),
        ('type', 'aspect',),
        'ski_pack',
        ('c2c', 'skitour'),
        ('created', 'modified'),
        'comments',
    )

    list_filter = ('type', 'massif__name', 'massif__country', 'ski_level', 'ski_grade')
    search_fields = ('name',)
    list_per_page = 250
    list_max_show_all = 1000
