from django.db import models
from django.contrib.auth.models import AbstractUser
from django.forms import ValidationError
from django.utils.translation import gettext_lazy as _
from django.utils.functional import cached_property
from django.utils import timezone


# from geoposition.fields import GeopositionField


class Country(models.Model):
    name = models.CharField(max_length=100, blank=True, default=None, null=True)
    code = models.CharField(max_length=3, blank=True, default=None, null=True)

    def __str__(self):
        return f'{self.name}'


class Massif(models.Model):
    class Meta:
        ordering = ['country']

    massif_id = models.AutoField(primary_key=True, serialize=False, verbose_name='massif_id')
    name = models.CharField(max_length=100, blank=True, default=None, null=True)
    description = models.CharField(max_length=100, blank=True, default=None, null=True)
    country = models.ForeignKey(Country, blank=True, null=True,
                                on_delete=models.SET_NULL)
    avalanche = models.URLField(blank=True, default='', null=True)
    created = models.DateTimeField(blank=True, null=True)
    modified = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return f'{self.country.name} / {self.name}'


class PlaceType(models.Model):
    name = models.CharField(max_length=50, blank=False)

    def __str__(self):
        return f'{self.name}'


class SkiGrade(models.Model):
    name = models.CharField(max_length=50, blank=False)
    description = models.CharField(max_length=100, blank=True, default=None, null=True)

    def __str__(self):
        return f'{self.name}'


class SlopeGrade(models.Model):
    name = models.CharField(max_length=50, blank=False)
    description = models.CharField(max_length=100, blank=True, default=None, null=True)

    def __str__(self):
        return f'{self.name}'


class SlopeAspect(models.Model):
    name = models.CharField(max_length=50, blank=False)
    description = models.CharField(max_length=100, blank=True, default=None, null=True)

    def __str__(self):
        return f'{self.name}'


class SkiLevel(models.Model):
    name = models.CharField(max_length=50, blank=False)
    description = models.CharField(max_length=100, blank=True, default=None, null=True)

    def __str__(self):
        return f'{self.name}'


class SkiTourerPack(models.Model):
    name = models.CharField(max_length=50, blank=False)
    comments = models.TextField(max_length=10000, blank=True, default='', null=True)

    def __str__(self):
        return f'{self.name}'


class Place(models.Model):
    class Meta:
        ordering = ('massif__country__name', 'massif__name', 'name')

    place_id = models.AutoField(primary_key=True, serialize=False, verbose_name='place_id')
    secu_day = models.BooleanField(_('Security Day Place'), default=False,
                                   help_text=_('Place Suitable for the security day activities'))
    name = models.CharField(max_length=100, blank=False)
    description = models.TextField(max_length=500, blank=False)

    type = models.ForeignKey(PlaceType, blank=True, null=True,
                             on_delete=models.SET_NULL)
    massif = models.ForeignKey(Massif, blank=True, null=True,
                               on_delete=models.SET_NULL)
    comments = models.TextField(max_length=10000, blank=True, default='', null=True)

    nb_days = models.IntegerField(blank=True, null=True)

    map_number = models.CharField(max_length=50, blank=True, null=True)
    alt_summit = models.IntegerField(blank=True, null=True)
    alt_start = models.IntegerField(blank=True, null=True)
    cumulated_diff = models.IntegerField(blank=True, null=True)

    aspect = models.ForeignKey(SlopeAspect, blank=True, null=True,
                               on_delete=models.SET_NULL)
    ski_grade = models.ForeignKey(SkiGrade, blank=True, null=True,
                                  on_delete=models.SET_NULL)
    slope_grade = models.ForeignKey(SlopeGrade, blank=True, null=True,
                                    on_delete=models.SET_NULL)
    ski_level = models.ForeignKey(SkiLevel, blank=True, null=True,
                                  on_delete=models.SET_NULL)
    ski_pack = models.ForeignKey(SkiTourerPack, blank=True, null=True,
                                 on_delete=models.SET_NULL)

    lat = models.CharField(max_length=20, blank=True, null=True)
    long = models.CharField(max_length=20, blank=True, null=True)
    lat_wsg84 = models.CharField(max_length=20, blank=True, null=True)
    long_wsg84 = models.CharField(max_length=20, blank=True, null=True)

    created = models.DateTimeField(blank=True, null=True)
    modified = models.DateTimeField(blank=True, null=True)

    # external_links = models.ManyToManyField(OutingUrls, blank=True) ?
    c2c = models.URLField(blank=True, default='', null=True)
    skitour = models.URLField(blank=True, default='', null=True)

    #    links one to many
    #    geoPosition = GeopositionField()
    # + WGS84

    def __str__(self):
        return f'{self.massif} / {self.name}'

    @property
    def total_ski_grade(self):
        return '{}|{}'.format(self.ski_grade, self.slope_grade)

    @property
    def name_simple(self):
        return self.name
