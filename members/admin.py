from django.contrib import admin, messages
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.conf import settings
from django.db.models import Q, Max
from django.contrib import messages
from django.utils.translation import ngettext

from django.contrib import messages
from django.utils.translation import ngettext

from .models import *


@admin.register(Season)
class SeasonAdmin(admin.ModelAdmin):
    model = Season
    fields = [
        'name',
        'start',
        'end',
        'description'
    ]


@admin.register(Member)
class MemberAdmin(UserAdmin):

    def move_to_current_season(self, request, queryset):
        last_season = Season.objects.all().order_by('-end')[0]
        updated = queryset.update(season=last_season)
        self.message_user(request, ngettext(
            '%d moved to the current season.',
            '%d moved to the current season',
            updated,
        ) % updated, messages.SUCCESS)
        self.description = 'Move selected members to the active season'

    def move_to_season_last(self, request, queryset):
        last_season = Season.objects.all().order_by('end')[0]
        updated = queryset.update(season=last_season)
        self.message_user(request, ngettext(
            '%d moved to the last season.',
            '%d moved to the last season',
            updated,
        ) % updated, messages.SUCCESS)
        self.description = 'Move selected members to the last season'

    def set_inactive(self, request, queryset):
        updated = queryset.update(active=False)
        self.message_user(request, ngettext(
            '%d set inactive.',
            '%d set inactive',
            updated,
        ) % updated, messages.SUCCESS)
        self.description = 'Set users to INACTIVE status'

    def set_active(self, request, queryset):
        updated = queryset.update(active=True)
        self.message_user(request, ngettext(
            '%d set active.',
            '%d set active',
            updated,
        ) % updated, messages.SUCCESS)
        self.description = 'Set users to ACTIVE status'

    model = Member
    list_display = [
        '_dispname',
        'active',
        'instructor',
        'instructor_club',
        'email',
        'mobile',
        'season',
        'comments',
    ]
    list_filter = ('season', 'instructor', 'active','instructor_club', 'is_ext', 'is_sa',)
    list_per_page = 250
    list_max_show_all = 1000
    fieldsets = (
        (None, {'fields': (
            'username',
            'password',
        )}),
        (_('Member info'), {'fields': (
            'member_id',
            'first_name',
            'last_name',
            'season',
            'email',
            'mobile',
            'url_photo',
            'comments',
        )}),
        (_('Member settings'), {'fields': (
            'active',
            'instructor',
            'instructor_club',
            'skier',
            'is_ext',
            'is_sa',
        )}),
        (_('Permissions'), {'fields': (
            'is_staff',
            'is_superuser',
            'groups',
            'user_permissions',
        ), 'classes': ('collapse',)}),
        (_('Important dates'), {'fields': (
            'last_login',
            'date_joined',
        ), 'classes': ('collapse',)}),
    )
    readonly_fields = ('new_email','member_id')
    ordering = ('-season__end', '-instructor', 'last_name', 'first_name')
    actions = (move_to_current_season, move_to_season_last, set_active, set_inactive)

    def _dispname(self, obj):
        return '{} ({})'.format(obj.name, obj.username)
