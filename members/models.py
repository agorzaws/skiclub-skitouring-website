from django.db import models
from django.contrib.auth.models import AbstractUser
from django.forms import ValidationError
from django.utils.translation import gettext_lazy as _
from django.utils.functional import cached_property
from django.utils import timezone


class Season(models.Model):
    class Meta:
        ordering = ['-end']

    name = models.CharField(max_length=9, default=None)
    start = models.IntegerField(null=True)
    end = models.IntegerField(null=True)
    description = models.CharField(max_length=100, default='Migrated')

    def __str__(self):
        return f'{self.name}'


class Member(AbstractUser):
    class Meta:
        ordering = ['season__end', 'instructor', 'last_name']

    mb_id = models.IntegerField(null=True)  # ancient member ID
    member_id = models.AutoField(primary_key=True, serialize=False, verbose_name='member_id', )

    new_email = models.EmailField(_('New email address'), blank=True, null=True,
                                  help_text=_(
                                      'Set if the user has requested to change their email address but has not yet confirmed it.'))

    mobile = models.CharField(max_length=13, blank=True, default=None, null=True)

    is_ext = models.BooleanField(_('external member'), default=False)
    is_sa = models.BooleanField(_('staff association'), default=False)
    is_email_hidden = models.BooleanField(_('e-mail address hidden'),
                                          default=False,
                                          help_text=_(
                                              'The member\'s e-mail address will not be shown in the booking sheet.'))

    dva = models.BooleanField(_('DVA'), default=True)
    showel = models.BooleanField(_('Shovel'), default=True)
    probe = models.BooleanField(_('Probe'), default=True)

    created = models.DateTimeField(blank=True, null=True)
    modified = models.DateTimeField(blank=True, null=True)

    instructor = models.BooleanField(_('MF'), default=False)
    instructor_club = models.BooleanField(_('MF_EXT'), default=False)
    active = models.BooleanField(_('active'), default=False)
    skier = models.BooleanField(_('skier'), default=False)

    season = models.ForeignKey(Season, blank=True, null=True, on_delete=models.SET_NULL)

    url_photo = models.TextField(max_length=10000, blank=True, null=True, default='')
    comments = models.TextField(max_length=10000, blank=True, default='', null=True)

    def __str__(self):
        return self.name

    def clean(self):
        self.username = self.username.lower()

    @cached_property
    def name(self):
        return f'{self.first_name} {self.last_name}'

    def __str__(self):
        return f'{self.name} ({self.email}) '


Member._meta.get_field('is_active').help_text = _('Unselect this instead of deleting members')
Member._meta.get_field('instructor').help_text = _('MF SkiTouring')
Member._meta.get_field('instructor_club').help_text = _('MF SkiAlplin/CrossCountry')
Member._meta.get_field('active').help_text = _('Set this to enable the user. If not set user cannot login')
Member._meta.get_field('skier').help_text = _('Active skier in the club')
Member._meta.get_field('is_ext').help_text = _('Set if not CERN affiliated (unused)')
Member._meta.get_field('is_sa').help_text = _('Set if member of the CERN Staff Association')
