import setuptools
from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="skiclubtouring",
    version="0.0.1",
    description="CERN ski touring club website.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.cern.ch/agorzaws/skiclub-skitouring-website",
    packages=setuptools.find_packages(),
)
